import 'package:ayo_jakarta/models/Kanal.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Plugins {
  static TabBarPlugin({List<Category> category, TabController tabController}) {
    return TabBar(
      isScrollable: true,
      unselectedLabelColor: Color.fromRGBO(127, 196, 253, 1),
      labelColor: Colors.white,
      labelStyle: TextStyle(fontWeight: FontWeight.bold),
      indicatorSize: TabBarIndicatorSize.tab,
      indicator: new BubbleTabIndicator(
        indicatorHeight: 35.0,
        indicatorColor: Color.fromRGBO(38, 153, 251, 1),
        tabBarIndicatorSize: TabBarIndicatorSize.tab,
      ),
      tabs: category
          .map((item) => Tab(
                  child: Row(
                children: <Widget>[
                  ImageIcon(
                  CachedNetworkImageProvider(item.icon)),
                  Text(" ${item.categoryName}"            
                  )
                ],
              )))
          .toList(),
      controller: tabController,
    );
  }

    final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  final icons = [
    Icons.directions_bike,
    Icons.directions_boat,
    Icons.directions_bus,
    Icons.directions_car,
    Icons.directions_railway,
    Icons.directions_run,
    Icons.directions_subway,
    Icons.directions_transit,
    Icons.directions_walk
  ];


  static String getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }
}
