import 'dart:async';
import 'dart:convert';
import 'package:ayo_jakarta/models/Kanal.dart';
import 'package:ayo_jakarta/models/ListData.dart';
import 'package:ayo_jakarta/models/Artikel.dart';

import 'package:http/http.dart' as http;

class API {
  static const baseUrl = "https://www.ayojakarta.com/api_mob";
  static const URL_IMAGE = "https://www.ayojakarta.com/images-jakarta/post/articles/";
  static Future<ListData> lisdata() async {
    var client = new http.Client();
    var url = baseUrl + "/getAll";
    var response = await client.get(url);
    var decodeJson = jsonDecode(response.body);
    return ListData.fromJson(decodeJson);
  }

  static Future<Kanal> fetchGetCategory() async {
    var client = new http.Client();
    var url = baseUrl+"/getKanal";
    var response = await client.get(url);
    var decodeJson = jsonDecode(response.body);
    return Kanal.fromJson(decodeJson);
  }

static Future<Artikel> fetchPosArtikel() async {
    var client = new http.Client();
    var url = baseUrl+"/getArticle";
    var response = await client.post(url);
    var decodeJson = jsonDecode(response.body);
    return Artikel.fromJson(decodeJson);
  }
}
