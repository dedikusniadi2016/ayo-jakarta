import 'dart:convert';
import 'dart:io';
import 'package:ayo_jakarta/models/artikel_detail.dart';
import 'package:path_provider/path_provider.dart';

class BookmarkStorage {
  
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/ayoBookmarks.dat');
  }

  Future<bool> writeBookmarks(List<ArticleDetails> bookmarksList) async {
    try {
      final file = await _localFile;

      String json = jsonEncode(bookmarksList);

      print("JSON writing to file: " + json);

      await file.writeAsString(json, mode: FileMode.write);

      return true;
    } catch (e) {
      print('error $e');
    }

    return false;
  }

  Future<List<ArticleDetails>> readBookmarks() async {
    try {
      final file = await _localFile;

      String jsonString = await file.readAsString();
      print("JSON reading from file: " + jsonString);
      Iterable jsonMap = jsonDecode(jsonString);

      List<ArticleDetails> books = jsonMap
          .map((parsedJson) => ArticleDetails.fromJson(parsedJson))
          .toList();

      return books;
    } catch (e) {
      print('error');
    }

    return List<ArticleDetails>();
  }
}