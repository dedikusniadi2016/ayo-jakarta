import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailsAboutPage extends StatefulWidget {
  String title;
  var text;

  DetailsAboutPage({this.title, this.text});

  @override
  _DetailsAboutPageState createState() => _DetailsAboutPageState();
}

class _DetailsAboutPageState extends State<DetailsAboutPage> {
  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: widget.text,
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: 15),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(widget.text);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          widget.title,
          style: TextStyle(
               fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
          child: _createHtmlView(context)),
    );
  }
}
