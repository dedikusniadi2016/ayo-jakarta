import 'package:ayo_jakarta/data/themes.dart';
import 'package:ayo_jakarta/data/uiset.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:launch_review/launch_review.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:ayo_jakarta/page/bookmark.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingPage> {
  
  bool _value2 = false;
  bool _value3 = false;

  void _onChanged2(bool value2) {
    setState(() {
      Provider.of<ThemeNotifier>(context).switchTheme();
      _value2 = value2;
    });
  }

    void _onChanged3(bool value3) {
    setState(() {
      _value3 = value3;
    });
  }

  Future<void> _ackAlert(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Image.asset('assets/ayojakarta.png',
              alignment: FractionalOffset.center),
          content: const Text(
              'Hak Cipta 2019 AyoJakarta.com,Semua Hak Dilindungi oleh undang undang'),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _launchURL(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    var ui = Provider.of<UiSet>(context);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Image.asset('assets/ayojakarta.png',
              height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        ),
        body: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(children: <Widget>[
              ListTile(
                title: Text("Bookmark"),
                leading: Icon(Icons.bookmark),
                subtitle: Text(
                    "Artikel yang Sudah Anda Simpan, dapat Anda baca kembali disini"),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Bookmark()));
                },
              ),
              SwitchListTile(
                  value: _value2,
                  title: Text("Dark Mode"),
                  activeColor: Colors.lightGreen,
                  secondary: Icon(Icons.brightness_4),
                  subtitle: Text(
                      "Aktifkan Mode Malam untuk Kenyamanan membaca dalam malam hari"),
                  onChanged: (bool value2) {
                    _onChanged2(value2);
                  }),
              SwitchListTile(
                value: _value3,
                title: Text("Notifikasi Artikel"),
                activeColor: Colors.red,
                secondary: Icon(Icons.notifications_active),
                subtitle: Text(
                    "Bantu Kami untuk selalu menghadirkan kualitas konten yang baik melalui kritik dan saran anda"),
                     onChanged: (bool value3) {
                    _onChanged3(value3);
                  }),
              ListTile(
                title: Text("kritik & saran"),
                leading: Icon(Icons.email),
                subtitle: Text(
                    "Aktifkan Notifikasi untuk Mendapatkan update artikel terbaru dan terhangat"),
                onTap: () => _launchURL(' jakarta@ayomedia.com',
                    'Kontak redaksi ayo jakarta', 'Kontak redaksi ayo jakarta'),
              ),
              ListTile(
                title: Text("Beri penilaian"),
                leading: Icon(Icons.star),
                subtitle: Text(
                    "Bantu kami untuk selalu menghadirkan pengalaman membaca yang baik dengan memberikan rating dan review di google play store"),
                onTap: () => LaunchReview.launch(
                  androidAppId: "com.ayobandung.news.developer.ayobandung",
                  iOSAppId: "585027354",
                ),
              ),
              ListTile(
                title: Text("tentang Ayo Jakarta"),
                leading: Icon(Icons.help),
                subtitle: Text(
                    "Informasi mengenai versi aplikasi dan copyright konten"),
                onTap: () {
                  _ackAlert(context);
                },
              ),
            ])));
  }
}
