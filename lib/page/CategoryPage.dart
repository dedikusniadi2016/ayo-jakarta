import 'package:ayo_jakarta/models/ArtikelModel.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import '../utility/other.dart';
import 'package:ayo_jakarta/page/DetailPage.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ayo_jakarta/models/ArtikelModel.dart';

class CategoryPage extends StatefulWidget {
  final String category;
  CategoryPage({this.category});

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Color _iconColor = Colors.black;

  Article article;
  int _totalRecent = 0;
  int _pageRecent = 0;
  String category;
  int ads = 1;

  Future<Article> _fetchGetRecentNews() async {
    category = widget.category;

    Response response = await Dio().post(
      "https://www.ayojakarta.com/api_mob/getRecentArticle",
      data: FormData.fromMap(
          {"cat_id": widget.category, "limit": 20, "page": _pageRecent}),
    );

    var decodeJson = jsonDecode(response.toString());

    if (identical(Article.fromJson(decodeJson).kode, 200)) {
      article == null
          ? article = Article.fromJson(decodeJson)
          : article.data.addAll(Article.fromJson(decodeJson).data);

      prefix0.Data data;

      if (identical(ads, 1)) {
        ads = 2;
        data = prefix0.Data(
            categoryName: "ads",
            postImageContent: article.top,
            slug: article.toplink);
      } else {
        ads = 1;
        data = prefix0.Data(
            categoryName: "ads",
            postImageContent: article.bottom,
            slug: article.bottomlink);
      }

      article.data.insert(
          identical(_pageRecent, 0) ? _pageRecent : (_pageRecent * 20), data);

      setState(() {
        _totalRecent = article.data.length;
      });
    }

    return article;
  }

  Future<Article> _handleRefresh() async {
    _pageRecent = 0;

    Response response = await Dio().post(
      "https://www.ayojakarta.com/api_mob/getRecentArticle",
      data: FormData.fromMap(
          {"cat_id": widget.category, "limit": 20, "page": _pageRecent}),
    );

    var decodeJson = jsonDecode(response.toString());

    article = Article.fromJson(decodeJson);

    prefix0.Data data = prefix0.Data(
        categoryName: "ads",
        postImageContent: article.top,
        slug: article.toplink);
    article.data.insert(_pageRecent, data);

    setState(() {
      _totalRecent = article.data.length;
    });

    return article;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  _createListView(Article article) {
    return RefreshIndicator(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: _totalRecent,
          itemBuilder: (BuildContext context, int index) {
            if (index >= _totalRecent - 1) {
              print(article.data[index].postTitle);
              _pageRecent++;
              _fetchGetRecentNews();
            }
            print(article.data[index].categoryName);
            return identical(article.data[index].categoryName, "ads")
                ? Container(
                    height: 40.1,
                    child: Image.network(
                      article.data[index].postImageContent,
                      fit: BoxFit.fill,
                      height: 40.1,
                      width: MediaQuery.of(context).size.width,
                    ),
                  )
                : Padding(
                    padding: EdgeInsets.only(left: 2.0, right: 2.0),
                    child: Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) => DetailPage(
                                      id: article.data[index].postId,
                                      tag: "${this.category}_$index",
                                      category:
                                          article.data[index].categoryName,
                                      image:
                                          "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                                    )));
                          },
                          child: Hero(
                              tag: "${this.category}_$index",
                              child: _buildListItem(article.data[index])),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Divider(
                            height: 1.0,
                            color: Colors.black26,
                          ),
                        ),
                      ],
                    ),
                  );
          }),
      onRefresh: _handleRefresh,
    );
  }

  Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: BoxFit.cover,
      );

  Widget _buildListItem(prefix0.Data item) => Material(
        type: MaterialType.transparency,
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                          ),
                          Image.asset('assets/logo-20.png'),
                          Text(
                            item.categoryName.toUpperCase(),
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            item.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            item.author == null
                                ? TextSpan()
                                : TextSpan(children: <TextSpan>[
                                    TextSpan(
                                      text: "Oleh ",
                                      style: TextStyle(
                                        fontSize: 11.0,
                                      ),
                                    ),
                                    TextSpan(
                                        text: "${item.author} ",
                                        style: TextStyle(
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ]),
                            TextSpan(
                              text: _getDateBetween(item.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(6.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                    height: 110.0,
                    width: 100.0,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildFutureView() => FutureBuilder(
      future: _fetchGetRecentNews(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _createListView(article);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: _status == true
            ? article == null ? _buildFutureView() : _createListView(article)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  @override
  void didUpdateWidget(CategoryPage oldWidget) {
    if (!identical(category, widget.category)) {
      setState(() {
        article = null;
        _totalRecent = 0;
        _pageRecent = 0;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
