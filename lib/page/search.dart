import 'package:ayo_jakarta/main.dart';
import 'package:ayo_jakarta/page/DetailPage.dart';
import 'package:ayo_jakarta/page/setting.dart';
import 'package:ayo_jakarta/utility/other.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ayo_jakarta/API/api.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:ayo_jakarta/data/uiset.dart';
import 'package:ayo_jakarta/data/themes.dart';
import 'package:provider/provider.dart';
import 'package:ayo_jakarta/data/uiset.dart';
import 'package:ayo_jakarta/data/themes.dart';
import 'package:ayo_jakarta/page/PhotoPage.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UiSet()),
      ChangeNotifierProvider(builder: (_) => ThemeNotifier()),
    ], child: SearchList()));

class SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'ayo jakarta',
      debugShowCheckedModeBanner: false,
      theme: Provider.of<ThemeNotifier>(context).curretThemeData,
      home: new ExamplePage(),
    );
  }
}

class ExamplePage extends StatefulWidget {
  @override
  _ExamplePageState createState() => new _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List post_titles = new List();
  List filteredTitle = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = Image.asset('assets/ayojakarta.png',
      height: 500.0, width: 200.0, alignment: FractionalOffset.center);

  _ExamplePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredTitle = post_titles;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomPadding: false,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BodyLayout()),
                  );
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.photo),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PhotoGalleryPage()));
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SettingPage()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredTitle.length; i++) {
        if (filteredTitle[i]['post_title']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredTitle[i]);
        }
      }
      filteredTitle = tempList;
    }
    return ListView.builder(
      itemCount: post_titles == null ? 0 : filteredTitle.length,
      itemBuilder: (BuildContext context, int index) {
        var listTile = new ListTile(
          trailing: new Image.network(
            "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('MM').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('dd').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${filteredTitle[index]['post_id']}/${filteredTitle[index]['post_image_content']}",
            fit: BoxFit.cover,
            height: 70.0,
            width: 70.0,
          ),
          title: Text(filteredTitle[index]['post_title']),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => DetailPage(
                      id: filteredTitle[index]['post_id'],
                      category:
                          filteredTitle[index]['category_name'].toUpperCase(),
                      tag: filteredTitle[index]['post_id'],
                      image:
                          "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('MM').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('dd').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${filteredTitle[index]['post_id']}/${filteredTitle[index]['post_image_content']}",
                    )));
          },
          subtitle: Text(DateFormat('dd-MMMM-yyyy').format(
              DateTime.parse(filteredTitle[index]['post_date_created']))),
        );
        return new Card(
          child: listTile,
          margin: const EdgeInsets.all(5.0),
        );
      },
    );
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = Image.asset('assets/ayojakarta.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center);
        filteredTitle = post_titles;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    final response =
        await dio.get('https://www.ayojakarta.com/api_mob/getSearch');
    List tempList = new List();
    for (int i = 0; i < response.data['data'].length; i++) {
      tempList.add(response.data['data'][i]);
    }
    setState(() {
      post_titles = tempList;
      post_titles.shuffle();
      filteredTitle = post_titles;
    });
  }
}
