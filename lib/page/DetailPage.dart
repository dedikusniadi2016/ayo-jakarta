import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:ayo_jakarta/models/artikel_detail.dart';
import 'package:ayo_jakarta/utility/other.dart';
import 'package:ayo_jakarta/API/api.dart';

class DetailPage extends StatefulWidget {
  final String id;
  final String tag;
  String image;
  String category;

  DetailPage({this.id, this.tag, this.image, this.category});

  @override
  _ContentPageState createState() => _ContentPageState();
}

enum TtsState { playing, stopped }

class _ContentPageState extends State<DetailPage>
    with TickerProviderStateMixin {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  ArticleDetails _details;

  String _image;
  String _category;
  bool _collapse = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  AnimationController _controller;
  Animation<double> _animation;

  Color _iconColor = Colors.white;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _controller.forward();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  Future<ArticleDetails> _fetchGetArticle() async {
    print("id " + widget.id);
    Response response = await Dio().post(
      "${Other.BASE_URL}getArticle",
      data: FormData.fromMap({"id": widget.id}),
    );
    var decodeJson = jsonDecode(response.toString());
    if (identical(ArticleDetails.fromJson(decodeJson).kode, 200)) {
      _details = ArticleDetails.fromJson(decodeJson);

      if (widget.image == null) {
        setState(() {
          widget.image =
              "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}";

          widget.category = _details.result[0].categoryName;
        });
      }
    }

    return _details;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: "<p align=\"justify\">${_details.result[0].postContent}</p>",
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      onLinkTap: (url) {
        String _url = url.substring(0, url.lastIndexOf("/")) + "";
        String id = _url.substring(_url.lastIndexOf("/") + 1);
        if (url.startsWith("https://www.ayojakarta.com/")) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => DetailPage(
                id: id,
                tag: "category_$id",
              ),
            ),
          );
        } else {
          _launchURL(url);
        }
      },
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: 16),
    );
  }

  Widget _futureBuilderContent(BuildContext context) => FutureBuilder(
      future: _fetchGetArticle(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _details == null
                ? Container(
                    child: Center(
                      child: Text(
                        "data tidak ditemukan",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : _createListContent(context);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  _displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(
        content: Text('Artikel berhasil disimpan di halaman bookmarks'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Widget _buildSliverBar() => SliverAppBar(
        expandedHeight: 250.0,
        floating: false,
        pinned: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.bookmark, color: _iconColor),
            onPressed: () {
              setState(() {
                _displaySnackBar(context);
                _iconColor = Colors.red;
              });
            },
          ),
          PopupMenuButton(
            icon: Icon(Icons.text_fields, color: Colors.red),
            itemBuilder: (context) => [
              PopupMenuItem(
                child: Text("Kecil"),
              ),
              PopupMenuItem(
                child: Text("Sedang"),
              ),
              PopupMenuItem(
                child: Text("Besar"),
              ),
            ],
          ),
        ],
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: false,
          title: _collapse
              ? FadeTransition(
                  opacity: _animation,
                  child: Text(
                    widget.category == null ? "" : widget.category,
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                  ),
                )
              : Container(),
          background: Hero(
            tag: widget.tag,
            child: widget.image == null
                ? CircularProgressIndicator()
                : _cacheNetworkImage(
                    widget.image,
                    BoxFit.fill,
                    MediaQuery.of(context).size.height,
                    MediaQuery.of(context).size.width),
          ),
        ),
      );

  Widget _buildContentView(BuildContext context) => NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          _collapse = innerBoxIsScrolled;
          return <Widget>[
            _buildSliverBar(),
          ];
        },
        body: _status == true
            ? _futureBuilderContent(context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      );

  _createListViewRelated(Related article, BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: article.data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => DetailPage(
                              id: article.data[index].postId,
                              tag: "category_$index",
                              category: article.data[index].categoryName,
                              image:
                                  "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: "category_$index",
                      child: _buildListItem(article.data[index], context)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  _createListViewMust(NewsCategory article, BuildContext context) {
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: article.data
          .map((item) => Padding(
                padding: EdgeInsets.only(left: 2.0, right: 2.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => DetailPage(
                                  id: item.postId,
                                  tag: "category_${item.postId}",
                                  category: item.categoryName,
                                  image:
                                      "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                                )));
                      },
                      child: Hero(
                          tag: "category_${item.postId}",
                          child: _buildListItem(item, context)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Divider(
                        height: 1.0,
                        color: Colors.black26,
                      ),
                    ),
                  ],
                ),
              ))
          .toList(),
    );
  }

  Widget _buildListItem(Data item, BuildContext context) => Material(
        child: Container(
          height: 100.0,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.network(
                      "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                      height: 110.0,
                      width: 100.0,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Image.asset('assets/logo-20.png'),
                            Text(
                              item.categoryName,
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 1.0),
                          child: SizedBox(
                            height: 38.0,
                            child: Text(
                              item.postTitle,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              item.author == null
                                  ? TextSpan()
                                  : TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: "Oleh ",
                                        style: TextStyle(
                                          fontSize: 11.0,
                                        ),
                                      ),
                                      TextSpan(
                                          text: "${item.author} ",
                                          style: TextStyle(
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ]),
                              TextSpan(
                                text: _getDateBetween(item.postDate),
                                style: TextStyle(
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 2),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  _createListContent(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        _details.result[0].postImageCaption == null
            ? Container()
            : Container(
                margin: EdgeInsets.only(bottom: 15.0),
                child: Text(
                  _details.result[0].postImageCaption,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 11.0),
                )),
        Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset('assets/logo-20.png'),
              Text(
                _details.result[0].categoryName,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0, left: 5.0),
          child: Text(
            _details.result[0].postTitle,
            style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                _details.result[0].reporter == null
                    ? TextSpan()
                    : TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: "Oleh ",
                          style: TextStyle(
                            fontSize: 11.0,
                          ),
                        ),
                        TextSpan(
                            text: "${_details.result[0].reporter} ",
                            style: TextStyle(
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            )),
                      ]),
                TextSpan(
                  text:
                      "- ${DateFormat.yMMMMEEEEd().format(DateTime.parse(_details.result[0].postDate))} ${DateFormat("Hms").format(DateTime.parse(_details.result[0].postDate))} WIB",
                  style: TextStyle(
                    fontSize: 11.0,
                  ),
                ),
              ],
            ),
          ),
        ),
        Divider(
          height: 3.0,
          color: Colors.black45,
        ),
        Container(
          margin: EdgeInsets.only(top: 15.0),
          height: 40.0,
          child: _cacheNetworkImage(
              _details.top,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        _createHtmlView(context),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(children: <TextSpan>[
                  TextSpan(
                    text: "Editor :  ",
                    style: TextStyle(
                      fontSize: 11.0,
                    ),
                  ),
                  TextSpan(
                      text: "${_details.result[0].author}",
                      style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.bold,
                      )),
                ]),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(children: <TextSpan>[
                  TextSpan(
                    text: "Tag :  ",
                    style: TextStyle(
                      fontSize: 11.0,
                    ),
                  ),
                  TextSpan(
                      text:  "${_details.result[0].post_keyword}",
                      style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.bold,
                      )),
                ]),
              ],
            ),
          ),
        ),
        Container(
          height: 40.0,
          child: _cacheNetworkImage(
              _details.bottom,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        identical(_details.related.kode, 502) == true
            ? Container()
            : _createTitle("Berita Terkait"),
        identical(_details.related.kode, 502) == true
            ? Container()
            : Container(
                height: (_details.related.data.length * 106).toDouble(),
                child: _createListViewRelated(_details.related, context),
              ),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : _createTitle(widget.category),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : Container(
                height: (_details.newsCategory.data.length * 106).toDouble(),
                child: _createListViewMust(_details.newsCategory, context),
              ),
      ],
    );
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          Divider(
            height: 3.0,
            color: Colors.black45,
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: _buildContentView(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Share.share(
              "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].slug}");
        },
        child: Icon(Icons.share, color: Colors.white),
        backgroundColor: Colors.blue,
      ),
    );
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
