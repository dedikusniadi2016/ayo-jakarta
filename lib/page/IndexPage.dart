import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:connectivity/connectivity.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../utility/other.dart';
import 'package:ayo_jakarta/models/Category.dart';
import 'package:ayo_jakarta/models/ArtikelModel.dart';
import 'package:ayo_jakarta/page/DetailPage.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int stat = 0;

  bool _status = false;

  Article _article;
  CategoryModel _category;
  Category selectedCategory;
  int _totalArticle = 0;
  int _totalCategory = 0;
  int _pageArticle = 1;

  final myController = TextEditingController();
  final controller = new MaskedTextController(mask: '00/00/0000');

  Future<Article> _fetchGetIndexNews() async {
    Response response = await Dio().post(
      "https://www.ayojakarta.com/api_mob/getIndex",
      data: FormData.fromMap({
        "cat_id": selectedCategory == null ? "0" : selectedCategory.categoryId,
        "date": controller.text == null ? getDatenow() : controller.text,
      }),
    );

    var decodeJson = jsonDecode(response.toString());

    if (identical(Article.fromJson(decodeJson).kode, 200)) {
      _article = Article.fromJson(decodeJson);

      setState(() {
        _totalArticle = _article.data.length;
      });
    }
    print(identical(Article.fromJson(decodeJson).kode, 200));

    return _article;
  }

  Future<CategoryModel> _fetchGetCategory() async {
    Response response = await Dio().get("${Other.BASE_URL}getKanal");
    var decodeJson = jsonDecode(response.toString());

    setState(() {
      _category = CategoryModel.fromJson(decodeJson);
      _totalCategory = _category.category.length;
      selectedCategory = _category.category[0];
    });

    print(_category.category[0].categoryName);

    return _category;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (controller.text == "") {
      controller.text = getDatenow();
    }

    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        if (_category == null) {
          _fetchGetCategory();
        }
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    controller.dispose();
    _connectivitySubscription.cancel();
    super.dispose();
  }

  String getDatenow() {
    DateTime now = DateTime.now();
    String format = DateFormat("dd/MM/yyyy").format(now);
    return format;
  }

  String _fixFormatDate(String date) {
    if (date.length == 1) {
      return "0$date";
    } else {
      return date;
    }
  }

  Widget _sliverCustum() => NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              expandedHeight: 200.0,
              floating: false,
              flexibleSpace: FlexibleSpaceBar(
                background: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextField(
                        controller: controller,
                        decoration: InputDecoration(
                          labelText: "Tanggal",
                          hintText: "dd/mm/yyyy",
                          suffixIcon: IconButton(
                            icon: Icon(Icons.calendar_today),
                            onPressed: () {
                              DatePicker.showDatePicker(
                                context,
                                showTitleActions: true,
                                locale: 'id',
                                minYear: 2000,
                                maxYear: DateTime.now().year,
                                initialYear: DateTime.now().year,
                                initialMonth: DateTime.now().month,
                                initialDate: DateTime.now().day,
                                cancel: Text(
                                  "batal",
                                  style: TextStyle(color: Colors.red),
                                ),
                                confirm: Text("tambahkan"),
                                dateFormat: 'dd-mmm-yyyy',
                                onConfirm: (year, month, date) {
                                  setState(() {
                                    controller.text =
                                        "${_fixFormatDate("$date")}/${_fixFormatDate("$month")}/$year";
                                  });
                                },
                              );
                            },
                          ),
                        ),
                      ),
                      Container(
                          margin: const EdgeInsets.only(top: 20.0),
                          child: _category == null
                              ? Container()
                              : _buildDropDownView()),
                      Container(
                        margin: EdgeInsets.only(top: 20.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Colors.orange,
                          elevation: 6.0,
                          onPressed: () {
                            if (_status) {
                              setState(() {
                                _article = null;
                              });
                            } else {
                              _scaffoldKey.currentState.showSnackBar(snackBar);
                            }
                          },
                          child: Text(
                            "Cari!",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ];
        },
        body: _article == null
            ? Container(
                child: Center(
                  child: Text(
                    "Index tidak ditemukan.",
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
              )
            : _createListView(_article),
      );

  Widget _buildDropDownView() => DropdownButton<Category>(
        hint: Text("All"),
        isDense: true,
        isExpanded: true,
        items: _category.category.map((Category item) {
          return DropdownMenuItem<Category>(
            child: Text(item.categoryName),
            value: item,
          );
        }).toList(),
        onChanged: (value) {
          setState(() {
            selectedCategory = value;
          });
        },
        value: selectedCategory,
      );

  Widget _createListView(Article article) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: _totalArticle,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => DetailPage(
                              id: article.data[index].postId,
                              tag: "category_$index",
                              category: article.data[index].categoryName,
                              image:
                                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: "category_$index",
                      child: _buildListItem(article.data[index])),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: BoxFit.fill,
      );

  Widget _buildListItem(Data item) => Material(
        child: Container(
          height: 100.0,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Container(
                    width: 100.0,
                    height: 100.0,
                    child: Material(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      elevation: 2.0,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      type: MaterialType.transparency,
                      child: Image.network(
                        "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                        height: 150,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(right: 5.0),
                            ),
                            Image.asset('assets/logo-20.png'),
                            Text(
                              item.categoryName,
                              style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.red,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 1.0),
                          child: SizedBox(
                            height: 38.0,
                            child: Text(
                              item.postTitle,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: RichText(
                          text: TextSpan(
                            //style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              item.author == null
                                  ? TextSpan()
                                  : TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: "Oleh ",
                                        style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 11.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      TextSpan(
                                          text: "${item.author} ",
                                          style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ]),
                              TextSpan(
                                text: _getDateBetween(item.postDate),
                                style: TextStyle(
                                  fontSize: 11.0,
                                  color: Colors.grey,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 8.0, right: 3.0),
                ),
              ],
            ),
          ),
        ),
      );

  Widget _buildFutureView() => FutureBuilder(
      future: _fetchGetIndexNews(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _sliverCustum();
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/ayojakarta.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center),
      ),
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: _status == true
          ? _article == null ? _buildFutureView() : _sliverCustum()
          : Container(
              child: Center(
                child: Text("Connection Failed"),
              ),
            ),
    );
  }

  _checkConnectivity() {}
}
