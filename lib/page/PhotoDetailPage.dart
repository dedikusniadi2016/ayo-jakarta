import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../models/photomodel.dart';
import '../utility/other.dart';
import 'package:photo_view/photo_view.dart';
import 'package:ayo_jakarta/page/HeroPhotoViewWrapper.dart';

class PhotoDetailPage extends StatefulWidget {
  final String id;

  PhotoDetailPage(this.id);

  @override
  _PhotoDetailPageState createState() => _PhotoDetailPageState();
}

class _PhotoDetailPageState extends State<PhotoDetailPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  PhotoModel _photoModel;
  int _totalPhoto = 0;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  get child => null;

  Future<PhotoModel> _fetchGetPhoto() async {
    Response response = await Dio().post(
      "${Other.BASE_URL}getPhoto",
      data: FormData.fromMap({"id": widget.id}),
    );
    var decodeJson = jsonDecode(response.toString());

    _photoModel = PhotoModel.fromJson(decodeJson);

    _totalPhoto = _photoModel.data[0].postImageContent.length;

    return _photoModel;
  }

  Widget _createPhotoitem(Data data, PostImageContent image) => Container(
        height: 290.0,
        child: Stack(
          children: <Widget>[
            ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: double.infinity, minHeight: double.infinity),
                child: 
                
                PhotoView(
                  imageProvider: NetworkImage(
                    "${Other.URL_PHOTOS}${DateFormat('yyyy').format(DateTime.parse(data.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(data.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(data.postDateCreated))}/${image.photoId}/${image.photoContent}",
                  ),
                ), 

                // child: Image.network(
                //   "${Other.URL_PHOTOS}${DateFormat('yyyy').format(DateTime.parse(data.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(data.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(data.postDateCreated))}/${image.photoId}/${image.photoContent}",
                //   fit: BoxFit.cover,
                //   width: MediaQuery.of(context).size.width,
                //   height: MediaQuery.of(context).size.height,
                // ),
                ),
          ],
        ),
      );

  Widget _buildSliderPhoto(PhotoModel _photo) {
    return PageIndicatorContainer(
      align: IndicatorAlign.bottom,
      length: _totalPhoto,
      indicatorColor: Colors.white,
      indicatorSelectorColor: Colors.red,
      size: 8.0,
      indicatorSpace: 5.0,
      pageView: PageView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _totalPhoto,
          controller: PageController(),
          itemBuilder: (BuildContext context, int index) {
            return _createPhotoitem(
                _photo.data[0], _photo.data[0].postImageContent[index]);
          }),
    );
  }

  Widget _createHtmlView() => Html(
        data: _photoModel.data[0].postImageCaption,
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        onLinkTap: (url) {},
        useRichText: true,
        defaultTextStyle: TextStyle(
          fontSize: 14.5,
          color: Colors.black,
        ),
      );

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 2),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildContentView(BuildContext context) => NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              expandedHeight: 290.0,
              floating: false,
              pinned: false,
              flexibleSpace: FlexibleSpaceBar(
                background: _buildSliderPhoto(_photoModel),
              ),
            ),
          ];
        },
        body: ListView(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(bottom: 15.0, top: 10.0),
                child: Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  child: Text(
                    _photoModel.data[0].postImageCaption,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 11.0),
                  ),
                )),
            Padding(
              padding: const EdgeInsets.only(bottom: 16.0, left: 5.0),
              child: Text(
                _photoModel.data[0].postTitle,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  children: <TextSpan>[
                    TextSpan(
                      text: "Oleh ",
                      style: TextStyle(
                        fontSize: 11.0,
                      ),
                    ),
                    TextSpan(
                        text: "${_photoModel.data[0].editor.toUpperCase()} ",
                        style: TextStyle(
                          fontSize: 11.0,
                          fontWeight: FontWeight.bold,
                        )),
                    TextSpan(
                      text:
                          "- ${DateFormat.yMMMMEEEEd().format(DateTime.parse(_photoModel.data[0].postDate))} ${DateFormat("Hms").format(DateTime.parse(_photoModel.data[0].postDate))} WIB",
                      style: TextStyle(
                        fontSize: 11.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              height: 3.0,
              color: Colors.black45,
            ),
            Container(
              margin: EdgeInsets.only(top: 15.0, bottom: 15.0),
              height: 40.0,
              child: _cacheNetworkImage(_photoModel.top, BoxFit.fitWidth, 40.0,
                  MediaQuery.of(context).size.width),
            ),
            Html(
              data: "${_photoModel.data[0].postContent} ",
            ),
            _createHtmlView(),
            Container(
              margin: EdgeInsets.only(top: 15.0),
              height: 40.0,
              child: _cacheNetworkImage(_photoModel.bottom, BoxFit.fitWidth,
                  40.0, MediaQuery.of(context).size.width),
            ),
          ],
        ),
      );

  Widget _futureBuildView(BuildContext context) => FutureBuilder(
      future: _fetchGetPhoto(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _buildContentView(context);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.bookmark),
            // onPressed: () {
            //   Share.share(
            //       "${Other.URL_READ_PHOTO}${DateFormat('yyyy').format(DateTime.parse(_photoModel.data[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_photoModel.data[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_photoModel.data[0].postDateCreated))}/${_photoModel.data[0].postId}/${_photoModel.data[0].slug}");
            // },
          )
        ],
      ),
      body: Container(
        child: _status == true
            ? _futureBuildView(context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
