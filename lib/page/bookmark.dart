import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ayo_jakarta/data/uiset.dart';
import 'package:ayo_jakarta/data/themes.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UiSet()),
      ChangeNotifierProvider(builder: (_) => ThemeNotifier()),
    ], child: Bookmark()));

class Bookmark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Provider.of<ThemeNotifier>(context).curretThemeData,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Image.asset('assets/ayojakarta.png',
              height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        ),
        body: ProfileBanner(),
      ),
    );
  }
}

class ProfileBanner extends StatelessWidget {
  get context => null;

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      child: Container(
        child: ListTile(
          leading: Image.network(
              "https://www.ayojakarta.com/images-jakarta/post/articles/2019/11/14/7857/anies_lagi_antaranews_thumb.jpg",
              fit: BoxFit.cover),
          trailing: Icon(Icons.arrow_left),
          title: Text('Anies Hadiri Rakornas PKS di Bidakara'),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'delete',
          color: Colors.red,
          icon: Icons.restore_from_trash,
          onTap: () => _onAlertButtonsPressed(context),
        ),
      ],
    );
  }

  _onAlertButtonsPressed(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Hapus Artikel",
      desc: "Yakin ingin menghapus 1 Artikel tersimpan",
      buttons: [
        DialogButton(
          child: Text(
            "Hapus",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.red
        ),
        DialogButton(
          child: Text(
            "urungkan",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.white,
        )
      ],
    ).show();
  }

  // void _settingModalBottomSheet(context) {
  //   showModalBottomSheet(
  //       context: context,
  //       builder: (BuildContext bc) {
  //         return Container(
  //           child: new Wrap(
  //             children: <Widget>[

  //               Container(
  //                 child: new Text("Yakin ingin menghapus 1 Artikel tersimpan"),
  //               ),
  //               Container(
  //                 alignment: Alignment(-0.6, -0.6),
  //                 child: new Text("X"),
  //               ),
  //             ],
  //           ),
  //         );
  //       });
  // }
}
