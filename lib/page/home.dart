import 'package:ayo_jakarta/API/api.dart';
import 'package:ayo_jakarta/models/Artikel.dart';
import 'package:ayo_jakarta/models/ListData.dart';
import 'package:ayo_jakarta/models/detail.dart';
import 'package:ayo_jakarta/page/DetailPage.dart';
import 'package:ayo_jakarta/page/PhotoDetailPage.dart';
import 'package:ayo_jakarta/plugins/Plugins.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ayo_jakarta/utility/other.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Artikel get detail => null;

  int heroTag = 0;

  @override
  Widget build(BuildContext context) {
    return _buildFutureView();
  }

  Widget _buildFutureView() => FutureBuilder(
      future: API.lisdata(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _buildView(snapshot.data);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });
  Widget _buildView(ListData data) => ListView(
        padding: const EdgeInsets.only(bottom: 50.0, top: 50.0),
        scrollDirection: Axis.vertical,
        children: <Widget>[
          _buildHeadlineView(data.headline),
          _createListView(data.recent),
          _createTitle("Popular"),
          _createListView2(data.popular),
          _createTitle("Photo"),
          _buildphotoView(data.photo),
          _createTitle("Persija"),
          _createListView3(data.persija),
        ],
      );

  Widget _buildHeadlineView(List<Headline> headline) => CarouselSlider(
        items: headline.map((item) => _createHeadline(item)).toList(),
        height: 200,
        viewportFraction: 0.9,
        aspectRatio: 2.0,
        initialPage: 0,
        enableInfiniteScroll: false,
        reverse: false,
        enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      );

  Widget _createHeadline(Headline item) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        child: Stack(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailPage(
                            id: item.postId,
                            image:
                                "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                            category: item.categoryName,
                            tag: item.postId)));
              },
              child: _cacheNetworkImage(
                  "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                  BoxFit.fill,
                  MediaQuery.of(context).size.height,
                  MediaQuery.of(context).size.width),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 75.0,
                decoration:
                    BoxDecoration(color: Colors.black12.withOpacity(0.3)),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 3.0, right: 3.0),
                      child: SizedBox(
                        height: 49.0,
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text: "${item.postTitle}",
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22.0,
              ),
            ),
          ),
          Divider(
            height: 3.0,
            color: Colors.black45,
          ),
        ],
      ),
    );
  }

  Widget _buildphotoView(List<Photo> photo) => CarouselSlider(
        items: photo.map((item) => _createPhoto(item)).toList(),
        height: 200,
        viewportFraction: 0.9,
        aspectRatio: 2.0,
        initialPage: 0,
        enableInfiniteScroll: false,
        reverse: false,
        enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      );

  Widget _createPhoto(Photo item) {
    return Container(
      margin: EdgeInsets.all(5.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            _cacheNetworkImage(
                "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                BoxFit.fill,
                MediaQuery.of(context).size.height,
                MediaQuery.of(context).size.width),
            Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Icon(Icons.photo_camera, color: Colors.white)),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 60.0,
                decoration:
                    BoxDecoration(color: Colors.black12.withOpacity(0.3)),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 3.0, right: 3.0),
                      child: SizedBox(
                        height: 45.0,
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text: "${item.postTitle}",
                                  style: TextStyle(
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _createListView(List<Artikel> data) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailPage(
                                id: data[index].postId,
                                image:
                                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(data[index].postDateCreated))}/${data[index].postId}/${data[index].postImageContent}",
                                category: data[index].categoryName,
                                tag: data[index].postId)));
                  },
                  child: Hero(
                      tag: index, child: _buildListItem(data[index], index)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildListItem(Artikel article, int index) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                          ),
                          Image.asset('assets/logo-20.png'),
                          Text(
                            article.categoryName.toUpperCase(),
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            article.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10.0, right: 2.0, left: 2.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            _textSpanReporter(article.author),
                            TextSpan(
                              text: Plugins.getDateBetween(article.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(6.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.postImageContent}",
                    height: 110.0,
                    width: 100.0,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  _textSpanReporter(String reporter) {
    if (reporter == null) {
      return TextSpan();
    } else {
      return TextSpan(children: <TextSpan>[
        TextSpan(
          text: "Oleh ",
          style: TextStyle(
            fontSize: 11.0,
          ),
        ),
        TextSpan(
            text: "${reporter.toUpperCase()} ",
            style: TextStyle(
              fontSize: 11.0,
              fontWeight: FontWeight.bold,
            )),
      ]);
    }
  }

  Widget _createListView2(List<Artikel> data) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailPage(
                                  id: data[index].postId,
                                  image:
                                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(data[index].postDateCreated))}/${data[index].postId}/${data[index].postImageContent}",
                                  category: data[index].categoryName,
                                  tag: data[index].postId,
                                )));
                  },
                  child: Hero(
                      tag: index, child: _buildListItem2(data[index], index)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildListItem2(Artikel article, int index) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                          ),
                          Image.asset('assets/logo-20.png'),
                          Text(
                            article.categoryName.toUpperCase(),
                            style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            article.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10.0, right: 2.0, left: 2.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            _textSpanReporter(article.author),
                            TextSpan(
                              text: Plugins.getDateBetween(article.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(6.0),
                child: _cacheNetworkImage(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.postImageContent}",
                    BoxFit.fill,
                    100.0,
                    100.0),
              ),
            ],
          ),
        ),
      );

  _textSpanReporter2(String reporter) {
    if (reporter == null) {
      return TextSpan();
    } else {
      return TextSpan(children: <TextSpan>[
        TextSpan(
          text: "Oleh ",
          style: TextStyle(
            fontSize: 11.0,
          ),
        ),
        TextSpan(
            text: "${reporter.toUpperCase()} ",
            style: TextStyle(
              fontSize: 11.0,
              fontWeight: FontWeight.bold,
            )),
      ]);
    }
  }

  Widget _createListView3(List<Artikel> data) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailPage(
                                  id: data[index].postId,
                                  image:
                                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(data[index].postDateCreated))}/${data[index].postId}/${data[index].postImageContent}",
                                  category: data[index].categoryName,
                                  tag: data[index].postId,
                                )));
                  },
                  child: Hero(
                      tag: index, child: _buildListItem3(data[index], index)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildListItem3(Artikel article, int index) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                          ),
                          Image.asset('assets/logo-20.png'),
                          Text(
                            article.categoryName.toUpperCase(),
                            style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            article.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10.0, right: 2.0, left: 2.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            _textSpanReporter(article.author),
                            TextSpan(
                              text: Plugins.getDateBetween(article.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(6.0),
                child: _cacheNetworkImage(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.postImageContent}",
                    BoxFit.fill,
                    100.0,
                    100.0),
              ),
            ],
          ),
        ),
      );

  _textSpanReporter3(String reporter) {
    if (reporter == null) {
      return TextSpan();
    } else {
      return TextSpan(children: <TextSpan>[
        TextSpan(
          text: "Oleh ",
          style: TextStyle(
            fontSize: 11.0,
          ),
        ),
        TextSpan(
            text: "${reporter.toUpperCase()} ",
            style: TextStyle(
              fontSize: 11.0,
              fontWeight: FontWeight.bold,
            )),
      ]);
    }
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );
}
