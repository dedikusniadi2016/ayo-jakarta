import 'package:ayo_jakarta/page/PhotoPage.dart';
import 'package:ayo_jakarta/page/search.dart';
import 'package:flutter/material.dart';
import 'package:ayo_jakarta/page/home.dart';
import 'package:ayo_jakarta/page/CategoryPage.dart';
import 'package:ayo_jakarta/page/setting.dart';


// ignore: must_be_immutable
class DynamicArticlePage extends StatelessWidget {
  int page;
  String category;

  DynamicArticlePage({this.page, this.category});
  @override
  Widget build(BuildContext context) {
    
   return page == 0 ? Home() : CategoryPage(category: category);
  }
}
