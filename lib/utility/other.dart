class Other{
  static const URL_IMAGE = "https://www.ayojakarta.com/images-jakarta/post/articles/";
  static const URL_PHOTOS = "https://www.ayojakarta.com/images-jakarta/post/photos/";
  static const BASE_URL = "https://www.ayojakarta.com/api_mob/";
  static const URL_WEBSITE = "https://www.ayojakarta.com/read/";
  static const URL_READ_PHOTO = "https://www.ayojakarta.com/view/";
}