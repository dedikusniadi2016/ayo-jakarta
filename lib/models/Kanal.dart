class Kanal {
  String _status;
  int _kode;
  List<Category> _category;

  Kanal({String status, int kode, List<Category> category}) {
    this._status = status;
    this._kode = kode;
    this._category = category;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Category> get category => _category;
  set category(List<Category> category) => _category = category;

  Kanal.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['category'] != null) {
      _category = new List<Category>();
      json['category'].forEach((v) {
        _category.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._category != null) {
      data['category'] = this._category.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Category {
  String _categoryId;
  String _categoryName;
  String _icon;

  Category({String categoryId, String categoryName, String icon}) {
    this._categoryId = categoryId;
    this._categoryName = categoryName;
    this._icon = icon;
  }

  String get categoryId => _categoryId;
  set categoryId(String categoryId) => _categoryId = categoryId;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get icon => _icon;
  set icon(String icon) => _icon = icon;

  Category.fromJson(Map<String, dynamic> json) {
    _categoryId = json['category_id'];
    _categoryName = json['category_name'];
    _icon = json['icon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this._categoryId;
    data['category_name'] = this._categoryName;
    data['icon'] = this._icon;
    return data;
  }
}
