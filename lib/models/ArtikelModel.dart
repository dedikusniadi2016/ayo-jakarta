class Article {
  String _status;
  int _kode;
  List<Data> _data;
  String _top;
  String _toplink;
  String _bottom;
  String _bottomlink;

  Article(
      {String status,
        int kode,
        List<Data> data,
        String top,
        String toplink,
        String bottom,
        String bottomlink}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
    this._top = top;
    this._toplink = toplink;
    this._bottom = bottom;
    this._bottomlink = bottomlink;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;
  String get top => _top;
  set top(String top) => _top = top;
  String get toplink => _toplink;
  set toplink(String toplink) => _toplink = toplink;
  String get bottom => _bottom;
  set bottom(String bottom) => _bottom = bottom;
  String get bottomlink => _bottomlink;
  set bottomlink(String bottomlink) => _bottomlink = bottomlink;

  Article.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
    _top = json['top'];
    _toplink = json['toplink'];
    _bottom = json['bottom'];
    _bottomlink = json['bottomlink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    data['top'] = this._top;
    data['toplink'] = this._toplink;
    data['bottom'] = this._bottom;
    data['bottomlink'] = this._bottomlink;
    return data;
  }
}

class Data {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _slug;
  String _categoryName;
  String _postImageContent;
  String _author;

  Data(
      {String postId,
        String postDate,
        String postDateCreated,
        String postTitle,
        String slug,
        String categoryName,
        String postImageContent,
        String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._slug = slug;
    this._categoryName = categoryName;
    this._postImageContent = postImageContent;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get author => _author;
  set author(String author) => _author = author;

  Data.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _slug = json['slug'];
    _categoryName = json['category_name'];
    _postImageContent = json['post_image_content'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['slug'] = this._slug;
    data['category_name'] = this._categoryName;
    data['post_image_content'] = this._postImageContent;
    data['author'] = this._author;
    return data;
  }
}
