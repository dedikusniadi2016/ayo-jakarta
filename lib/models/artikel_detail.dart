class ArticleDetails {

  String _status;
  int _kode;
  List<Result> _result;
  String _top;
  String _toplink;
  String _bottom;
  String _bottomlink;
  HitsStatus _hitsStatus;
  Related _related;
  NewsCategory _newsCategory;

  ArticleDetails
  ({
      String status,
      int kode,
      List<Result> result,
      String top,
      String toplink,
      String bottom,
      String bottomlink,
      HitsStatus hitsStatus,
      Related related,
      NewsCategory newsCategory
    }) {
    this._status = status;
    this._kode = kode;
    this._result = result;
    this._top = top;
    this._toplink = toplink;
    this._bottom = bottom;
    this._bottomlink = bottomlink;
    this._hitsStatus = hitsStatus;
    this._related = related;
    this._newsCategory = newsCategory;
  }

  String get status => _status;
  set status(String status) => _status = status;
  
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  
  List<Result> get result => _result;
  set result(List<Result> result) => _result = result;
  
  String get top => _top;
  set top(String top) => _top = top;
  
  String get toplink => _toplink;
  set toplink(String toplink) => _toplink = toplink;
  
  String get bottom => _bottom;
  set bottom(String bottom) => _bottom = bottom;
  
  String get bottomlink => _bottomlink;
  set bottomlink(String bottomlink) => _bottomlink = bottomlink;
  
  HitsStatus get hitsStatus => _hitsStatus;
  set hitsStatus(HitsStatus hitsStatus) => _hitsStatus = hitsStatus;
  
  Related get related => _related;
  set related(Related related) => _related = related;
  
  NewsCategory get newsCategory => _newsCategory;
  set newsCategory(NewsCategory newsCategory) => _newsCategory = newsCategory;

  ArticleDetails.fromJson(Map<String, dynamic> json) {
  
    _status = json['status'];
    _kode = json['kode'];
  
    if (json['result'] != null) {
      _result = new List<Result>();
      json['result'].forEach((v) {
        _result.add(new Result.fromJson(v));
      });
    }
  
    _top = json['top'];
    _toplink = json['toplink'];
    _bottom = json['bottom'];
    _bottomlink = json['bottomlink'];
    _hitsStatus = json['hits_status'] != null
        ? new HitsStatus.fromJson(json['hits_status'])
        : null;
    _related =
        json['related'] != null ? new Related.fromJson(json['related']) : null;
    _newsCategory = json['newsCategory'] != null
        ? new NewsCategory.fromJson(json['newsCategory'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
   
    if (this._result != null) {
      data['result'] = this._result.map((v) => v.toJson()).toList();
    }
   
    data['top'] = this._top;
    data['toplink'] = this._toplink;
    data['bottom'] = this._bottom;
    data['bottomlink'] = this._bottomlink;
   
    if (this._hitsStatus != null) {
      data['hits_status'] = this._hitsStatus.toJson();
    }
   
    if (this._related != null) {
      data['related'] = this._related.toJson();
    }
    if (this._newsCategory != null) {
      data['newsCategory'] = this._newsCategory.toJson();
    }
    return data;
  }
}

class Result {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postContent;
  String _postImageContent;
  String _postImageCaption;
  String _slug;
  String _postStatus;
  String _postType;
  String _postKeyword;
  String _author;
  String _post_keyword;
  String _reporter;
  String _categoryId;
  String _categoryName;
  String _postSource;

  Result(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String postContent,
      String postImageContent,
      String postImageCaption,
      String slug,
      String postStatus,
      String postType,
      String postKeyword,
      String author,
      String post_keyword,
      String reporter,
      String categoryId,
      String categoryName,
      String postSource}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postContent = postContent;
    this._postImageContent = postImageContent;
    this._postImageCaption = postImageCaption;
    this._slug = slug;
    this._postStatus = postStatus;
    this._postType = postType;
    this._postKeyword = postKeyword;
    this._post_keyword = post_keyword;
    this._author = author;
    this._post_keyword = post_keyword;
    this._reporter = reporter;
    this._categoryId = categoryId;
    this._categoryName = categoryName;
    this._postSource = postSource;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postContent => _postContent;
  set postContent(String postContent) => _postContent = postContent;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get postImageCaption => _postImageCaption;
  set postImageCaption(String postImageCaption) =>
      _postImageCaption = postImageCaption;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get postStatus => _postStatus;
  set postStatus(String postStatus) => _postStatus = postStatus;
  String get postType => _postType;
  set postType(String postType) => _postType = postType;
  String get postKeyword => _postKeyword;
  set postKeyword(String postKeyword) => _postKeyword = postKeyword;

  String get author => _author;
  set author(String author) => _author = author;

  String get post_keyword => _post_keyword;
  set post_keyword(String post_keyword) => _post_keyword;

  String get reporter => _reporter;
  set reporter(String reporter) => _reporter = reporter;
  String get categoryId => _categoryId;
  set categoryId(String categoryId) => _categoryId = categoryId;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get postSource => _postSource;
  set postSource(String postSource) => _postSource = postSource;

  Result.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postContent = json['post_content'];
    _postImageContent = json['post_image_content'];
    _postImageCaption = json['post_image_caption'];
    _slug = json['slug'];
    _postStatus = json['post_status'];
    _postType = json['post_type'];
    _postKeyword = json['post_keyword'];
    _author = json['author'];
    _post_keyword = json['post_keyword'];
    _reporter = json['reporter'];
    _categoryId = json['category_id'];
    _categoryName = json['category_name'];
    _postSource = json['post_source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_content'] = this._postContent;
    data['post_image_content'] = this._postImageContent;
    data['post_image_caption'] = this._postImageCaption;
    data['slug'] = this._slug;
    data['post_status'] = this._postStatus;
    data['post_type'] = this._postType;
    data['post_keyword'] = this._postKeyword;
    data['author'] = this._author;
    data['post_keyword'] = this._post_keyword;
    data['reporter'] = this._reporter;
    data['category_id'] = this._categoryId;
    data['category_name'] = this._categoryName;
    data['post_source'] = this._postSource;
    return data;
  }
}

class HitsStatus {
  String _status;
  int _kode;
  bool _hits;

  HitsStatus({String status, int kode, bool hits}) {
    this._status = status;
    this._kode = kode;
    this._hits = hits;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  bool get hits => _hits;
  set hits(bool hits) => _hits = hits;

  HitsStatus.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    _hits = json['hits'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    data['hits'] = this._hits;
    return data;
  }
}

class Related {
  String _status;
  int _kode;
  List<Data> _data;

  Related({String status, int kode, List<Data> data}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;

  Related.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;
  String _slug;
  String _categoryName;
  String _author;
  String _post_keyword;

  Data(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String postImageContent,
      String slug,
      String categoryName,
      String author,
      String post_keyword
      }) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
    this._slug = slug;
    this._categoryName = categoryName;
    this._author = author;
    this._post_keyword = post_keyword;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  
  String get author => _author;
  set author(String author) => _author = author;

  String get post_keyword => _post_keyword;
  set post_keyword(String post_keyword) => _post_keyword = post_keyword;

  Data.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
    _slug = json['slug'];
    _categoryName = json['category_name'];
    _author = json['author'];
    _post_keyword = json['post_keyword'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    data['slug'] = this._slug;
    data['category_name'] = this._categoryName;
    data['author'] = this._author;
    data['post_keyword'] = this._post_keyword;
    return data;
  }
}

class NewsCategory {
  String _status;
  int _kode;
  List<Data> _data;

  NewsCategory({String status, int kode, List<Data> data}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;

  NewsCategory.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
