class Detail {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postContent;
  String _postImageContent;
  String _postImageCaption;
  String _slug;
  String _postStatus;
  String _postType;
  String _postKeyword;
  String _editor;
  String _reporter;
  String _categoryId;
  String _categoryName;
  String _postSource;

  Detail(bool Function(Detail) param0, 
      {
        String postId,
        String postDate,
        String postDateCreated,
        String postTitle,
        String postContent,
        String postImageContent,
        String postImageCaption,
        String slug,
        String postStatus,
        String postType,
        String postKeyword,
        String editor,
        String reporter,
        String categoryId,
        String categoryName,
        String postSource
    }) {
    
    this._postId            = postId;
    this._postDate          = postDate;
    this._postDateCreated   = postDateCreated;
    this._postTitle         = postTitle;
    this._postContent       = postContent;
    this._postImageContent  = postImageContent;
    this._postImageCaption  = postImageCaption;  
    this._slug              = slug;
    this._postStatus        = postStatus;
    this._postType          = postType;
    this._postKeyword       = postKeyword;
    this._editor            = editor;
    this._reporter          = reporter;
    this._categoryId        = categoryId;
    this._categoryName      = categoryName;
    this._postSource        = postSource;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  
  String get postContent => _postContent;
  set postContent(String postContent) => _postContent = postContent;
  
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) => _postImageContent = postImageContent;

  String get postImageCaption => _postImageCaption;
  set postImageCaption(String postImageCaption) => _postImageCaption = postImageCaption;

  String get slug => _slug;
  set slug(String slug) => _slug = slug;

  String get postStatus => _postStatus;
  set postStatus(String postStatus) => _postStatus = postStatus;

  String get postType => _postType;
  set postType(String postType) => _postType = postType;

  String get postKeyword => _postKeyword; 
  set postKeyword(String postKeyword) => _postKeyword = postKeyword;

  String get editor => _editor;
  set editor(String editor) => _editor = editor;

  String get reporter => _reporter;
  set reporter(String reporter) => _reporter = reporter;

  String get categoryId => _categoryId;
  set categoryId(String categoryId) => _categoryId = categoryId;

  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;

  String get postSource => _postSource;
  set postSource(String postSource) => _postSource = postSource;

  Detail.fromJson(Map<String, dynamic> json) {
    _postId           = json['post_id'];
    _postDate         = json['post_date'];
    _postDateCreated  = json['post_date_created'];
    _postTitle        = json['post_title'];
    _postContent      = json['post_content'];
    _postImageContent = json['post_image_content'];
    _postImageCaption = json['post_image_caption'];
    _slug             = json['slug'];
    _postStatus       = json['post_status'];
    _postType         = json['post_type'];
    _postKeyword      = json['post_keyword'];
    _editor           = json['editor'];
    _reporter         = json['reporter'];
    _categoryId       = json['category_id'];
    _categoryName     = json['category_name'];
    _postSource       = json['post_source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id']            = this._postId;
    data['post_date']          = this._postDate;
    data['post_date_created']  = this._postDateCreated;
    data['post_title']         = this._postTitle;
    data['post_content']       = this._postContent;
    data['post_image_content'] = this._postImageContent;
    data['post_image_caption'] = this._postImageCaption;
    data['slug']               = this._slug;
    data['post_status']        = this._postStatus;
    data['post_type']          = this._postType;
    data['post_keyword']       = this._postKeyword;
    data['editor']             = this._editor;
    data['reporter']           = this._reporter;
    data['category_id']        = this._categoryId;
    data['category_name']      = this._categoryName;
    data['post_source']        = this._postSource;
    return data;
  }
}