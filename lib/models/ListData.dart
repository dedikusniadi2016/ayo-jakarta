import 'package:ayo_jakarta/models/Artikel.dart';
import 'package:ayo_jakarta/models/Photo.dart';

class ListData {
  List<Headline> _headline;
  List<Artikel> _recent;
  List<Artikel> _popular;
  List<Artikel> _persija;
  List<Photo> _photo;
  String _imageAds1;
  String _imageAds2;
  String _imageAds3;
  String _imageAds4;
  String _imageAds5;
  String _adsLink1;
  String _adsLink2;
  String _adsLink3;
  String _adsLink4;
  String _adsLink5;
  double _adsSize1;
  double _adsSize2;
  double _adsSize3;
  double _adsSize4;
  double _adsSize5;

  ListData(
      {List<Headline> headline,
        List<Artikel> recent,
        List<Artikel> popular,
        List<Artikel> persija,
        List<Photo> photo,
        String imageAds1,
        String imageAds2,
        String imageAds3,
        String imageAds4,
        String imageAds5,
        String adsLink1,
        String adsLink2,
        String adsLink3,
        String adsLink4,
        String adsLink5,
        double adsSize1,
        double adsSize2,
        double adsSize3,
        double adsSize4,
        double adsSize5}) {
    this._headline = headline;
    this._recent = recent;
    this._popular = popular;
    this._persija = persija;
    this._photo = photo;
    this._imageAds1 = imageAds1;
    this._imageAds2 = imageAds2;
    this._imageAds3 = imageAds3;
    this._imageAds4 = imageAds4;
    this._imageAds5 = imageAds5;
    this._adsLink1 = adsLink1;
    this._adsLink2 = adsLink2;
    this._adsLink3 = adsLink3;
    this._adsLink4 = adsLink4;
    this._adsLink5 = adsLink5;
    this._adsSize1 = adsSize1;
    this._adsSize2 = adsSize2;
    this._adsSize3 = adsSize3;
    this._adsSize4 = adsSize4;
    this._adsSize5 = adsSize5;
  }

  List<Headline> get headline => _headline;
  set headline(List<Headline> headline) => _headline = headline;
  List<Artikel> get recent => _recent;
  set recent(List<Artikel> recent) => _recent = recent;
  List<Artikel> get popular => _popular;
  set popular(List<Artikel> popular) => _popular = popular;
  List<Artikel> get persija => _persija;
  set persija(List<Artikel> persija) => _persija = persija;
  List<Photo> get photo => _photo;
  set photo(List<Photo> photo) => _photo = photo;
  String get imageAds1 => _imageAds1;
  set imageAds1(String imageAds1) => _imageAds1 = imageAds1;
  String get imageAds2 => _imageAds2;
  set imageAds2(Null imageAds2) => _imageAds2 = imageAds2;
  String get imageAds3 => _imageAds3;
  set imageAds3(Null imageAds3) => _imageAds3 = imageAds3;
  String get imageAds4 => _imageAds4;
  set imageAds4(String imageAds4) => _imageAds4 = imageAds4;
  String get imageAds5 => _imageAds5;
  set imageAds5(String imageAds5) => _imageAds5 = imageAds5;
  String get adsLink1 => _adsLink1;
  set adsLink1(String adsLink1) => _adsLink1 = adsLink1;
  String get adsLink2 => _adsLink2;
  set adsLink2(String adsLink2) => _adsLink2 = adsLink2;
  String get adsLink3 => _adsLink3;
  set adsLink3(String adsLink3) => _adsLink3 = adsLink3;
  String get adsLink4 => _adsLink4;
  set adsLink4(String adsLink4) => _adsLink4 = adsLink4;
  String get adsLink5 => _adsLink5;
  set adsLink5(String adsLink5) => _adsLink5 = adsLink5;
  double get adsSize1 => _adsSize1;
  set adsSize1(double adsSize1) => _adsSize1 = adsSize1;
  double get adsSize2 => _adsSize2;
  set adsSize2(double adsSize2) => _adsSize2 = adsSize2;
  double get adsSize3 => _adsSize3;
  set adsSize3(double adsSize3) => _adsSize3 = adsSize3;
  double get adsSize4 => _adsSize4;
  set adsSize4(double adsSize4) => _adsSize4 = adsSize4;
  double get adsSize5 => _adsSize5;
  set adsSize5(double adsSize5) => _adsSize5 = adsSize5;

  ListData.fromJson(Map<String, dynamic> json) {
    if (json['headline'] != null) {
      _headline = new List<Headline>();
      json['headline'].forEach((v) {
        _headline.add(new Headline.fromJson(v));
      });
    }
    if (json['recent'] != null) {
      _recent = new List<Artikel>();
      json['recent'].forEach((v) {
        _recent.add(new Artikel.fromJson(v));
      });
    }
    if (json['popular'] != null) {
      _popular = new List<Artikel>();
      json['popular'].forEach((v) {
        _popular.add(new Artikel.fromJson(v));
      });
    }
    if (json['persija'] != null) {
      _persija = new List<Artikel>();
      json['persija'].forEach((v) {
        _persija.add(new Artikel.fromJson(v));
      });
    }
    if (json['photo'] != null) {
      _photo = new List<Photo>();
      json['photo'].forEach((v) {
        _photo.add(new Photo.fromJson(v));
      });
    }
    _imageAds1 = json['image_ads_1'];
    _imageAds2 = json['image_ads_2'];
    _imageAds3 = json['image_ads_3'];
    _imageAds4 = json['image_ads_4'];
    _imageAds5 = json['image_ads_5'];
    _adsLink1 = json['ads_link_1'];
    _adsLink2 = json['ads_link_2'];
    _adsLink3 = json['ads_link_3'];
    _adsLink4 = json['ads_link_4'];
    _adsLink5 = json['ads_link_5'];
    _adsSize1 = json['ads_size_1'];
    _adsSize2 = json['ads_size_2'];
    _adsSize3 = json['ads_size_3'];
    _adsSize4 = json['ads_size_4'];
    _adsSize5 = json['ads_size_5'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._headline != null) {
      data['headline'] = this._headline.map((v) => v.toJson()).toList();
    }
    if (this._recent != null) {
      data['recent'] = this._recent.map((v) => v.toJson()).toList();
    }
    if (this._popular != null) {
      data['popular'] = this._popular.map((v) => v.toJson()).toList();
    }
    if (this._persija != null) {
      data['persija'] = this._persija.map((v) => v.toJson()).toList();
    }
    if (this._photo != null) {
      data['photo'] = this._photo.map((v) => v.toJson()).toList();
    }
    data['image_ads_1'] = this._imageAds1;
    data['image_ads_2'] = this._imageAds2;
    data['image_ads_3'] = this._imageAds3;
    data['image_ads_4'] = this._imageAds4;
    data['image_ads_5'] = this._imageAds5;
    data['ads_link_1'] = this._adsLink1;
    data['ads_link_2'] = this._adsLink2;
    data['ads_link_3'] = this._adsLink3;
    data['ads_link_4'] = this._adsLink4;
    data['ads_link_5'] = this._adsLink5;
    data['ads_size_1'] = this._adsSize1;
    data['ads_size_2'] = this._adsSize2;
    data['ads_size_3'] = this._adsSize3;
    data['ads_size_4'] = this._adsSize4;
    data['ads_size_5'] = this._adsSize5;
    return data;
  }
}

class Headline {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;
  String _categoryName;
  String _author;

  Headline(
      {String postId,
        String postDate,
        String postDateCreated,
        String postTitle,
        String postImageContent,
        String categoryName,
        String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
    this._categoryName = categoryName;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get author => _author;
  set author(String author) => _author = author;

  Headline.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
    _categoryName = json['category_name'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    data['category_name'] = this._categoryName;
    data['author'] = this._author;
    return data;
  }
}

class Photo {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;

  Photo(
      {String postId,
        String postDate,
        String postDateCreated,
        String postTitle,
        String postImageContent}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;

  Photo.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    return data;
  }
}